(function () {
    "use strict";

    function Rectangle(width, height) {
        this.width = width;
        this.height = height;
        this._calcArea = function () {
            return this.width * this.height;
        };
    }

    Rectangle.prototype.area = function () {
        return this._calcArea()
    };

    var times = [];
    var instances = [];

    for (var i = 0, l = 100000; i < l; i++) {
        var t0 = performance.now();
        var p = new Rectangle(200, 100);
        var t1 = performance.now();
        instances.push(p);
        times.push(t1 - t0);
    }

    var timesSum = times.reduce(function (prev, curr) {
        return prev + curr;
    });

    console.log('AVERAGE INSTANTIATE TIME WITH CONSTRUCTOR');
    console.log(timesSum / times.length);
    console.log('----------------------------------------');


})();
