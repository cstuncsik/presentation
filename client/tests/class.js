(function () {
    "use strict";

    class Rectangle {
        constructor(width, height) {
            this.width = width;
            this.height = height;
        }

        get area() {
            return this.calcArea()
        }

        calcArea() {
            return this.width * this.height;
        }
    }

    let times = [];
    let instances = [];

    for (var i = 0, l = 100000; i < l; i++) {
        let t0 = performance.now();
        let p = new Rectangle(200, 100);
        let t1 = performance.now();
        instances.push(p);
        times.push(t1 - t0);
    }

    let timesSum = times.reduce((prev, curr) => prev + curr);

    console.log('AVERAGE INSTANTIATE TIME WITH CLASS');
    console.log(timesSum / times.length);
    console.log('----------------------------------------');

})();

