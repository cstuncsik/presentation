(function () {
    "use strict";

    var rectProto = {
        area: function () {
            return this.calcArea()
        }
    };

    function Rectangle(width, height) {
        var rect = Object.create(rectProto);
        rect.width = width;
        rect.height = height;
        rect._calcArea = function () {
            return this.width * this.height;
        };
        return rect;
    }

    var times = [];
    var instances = [];

    for (var i = 0, l = 100000; i < l; i++) {
        var t0 = performance.now();
        var p = Rectangle(200, 100);
        var t1 = performance.now();
        instances.push(p);
        times.push(t1 - t0);
    }

    var timesSum = times.reduce(function (prev, curr) {
        return prev + curr;
    });

    console.log('AVERAGE INSTANTIATE TIME WITH FACTORY');
    console.log(timesSum / times.length);
    console.log('----------------------------------------');

})();
