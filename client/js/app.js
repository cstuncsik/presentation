const ui = impress();
const socket = io.connect();

hljs.initHighlightingOnLoad();
ui.init();

socket.on('connect', () => {
    console.log('connected');
});

socket.on('uievent', data => {
    switch(data.action){
        case 'swipeleft':
            ui.next();
            break;
        case 'swiperight':
            ui.prev();
            break;

    }
});
