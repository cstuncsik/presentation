export default {
    get: {
        id: id => document.getElementById(id),
        cls: cls => document.getElementsByClassName(cls),
        qs: qs => document.querySelector(qs),
        qsa: qsa => document.querySelectorAll(qsa)
    }
};
