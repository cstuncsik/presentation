import dom from './utils/dom';

const socket = io.connect();
const body = new Hammer(dom.get.qsa('body')[0]);

socket.on('connect', () => {
    console.log('connected');
});

socket.on('disconnect', () => {
    console.log('disconnected');
});

body.on('swipeleft', () => {
    socket.emit('uievent', {action: 'swipeleft'});
});

body.on('swiperight', () => {
    socket.emit('uievent', {action: 'swiperight'});
});
