// A more appropriate implementation
var partialApply = function (fn) {
    // Return a function which accepts the preloaded arguments
    return function () {
        var args = Array.prototype.slice.call(arguments);
        // Return another function which accepts the remaining arguments
        return function () {
            return fn.apply(this, args.concat.apply(args, arguments));
        };
    };
};
var computePartialApply = partialApply(function (a, b, c) {
    return a * b + c;
});

var computeWithLessParam = computePartialApply(2); // function (b,c)
console.log(computeWithLessParam(3, 4)); // 10

var computeWithLastParam = computePartialApply(2, 3); // function (c)
console.log(computeWithLastParam(4)); // 10

var curry = function (fn) {
    var args = Array.prototype.slice.call(arguments);
    // Since we call curry recursively if it has enough arguments to run
    // just cut out the first one which was the curried function actually
    // and call it with the rest (all the arguments it needs)
    if (args.length - 1 >= fn.length) return fn.apply(this, args.slice(1));
    // If there are not enough arguments just return a function waiting for the rest
    return function () {
        return curry.apply(this, args.concat.apply(args, arguments));
    };
};
var compose = function compose() {
    var functions = Array.prototype.slice.call(arguments);
    return functions.reduce(function(f, g) {
        return function() {
            return f(g.apply(this, arguments));
        };
    });
};
var lorem = 'Lorem ipsum dolor sit amet,\n' +
    'consectetur adipiscing elit.\n' +
    'Aenean ipsum tortor, dictum et viverra ut,\n' +
    'quis mattis nisl magna nec lorem';
var replace = curry(function(find, replacement, str) {
    var regex = new RegExp(find, 'g');
    return str.replace(regex, replacement);
});
var wrapWith = curry(function(tag, str) {
    return '<' + tag + '>' + str + '</' + tag + '>';
});
var htmlifyLorem = compose(
    wrapWith('blockquote'),
    wrapWith('p'),
    replace('\n', '<br/>'),
    replace('ipsum', wrapWith('em', 'sodales'))
);
console.log(htmlifyLorem(lorem));
