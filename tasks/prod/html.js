import config from '../config.js';
import gulp from 'gulp';
import util from 'gulp-util';
import usemin from 'gulp-usemin';
import cssnano from 'gulp-cssnano';
import uglify from 'gulp-uglify';
import glob from 'glob';
import eventStream from 'event-stream';

gulp.task('html:prod', done => {
    glob(config.paths.builds.tmp.root + '*.html', (err, files) => {
        if (err) {
            done(err);
        }

        let tasks = files.map(file => {
            return gulp.src(file)
                .pipe(usemin({
                    css: [cssnano()],
                    js: [uglify()]
                }))
                .pipe(gulp.dest(config.paths.builds.tmp.root))
                .on('error', util.log);
        });
        eventStream.merge(tasks).on('end', done);
    });
});
