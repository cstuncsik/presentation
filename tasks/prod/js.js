import config from '../config.js';
import gulp from 'gulp';
import util from 'gulp-util';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import browserify from 'browserify';
import babelify from 'babelify';
import glob from 'glob';
import eventStream from 'event-stream';

gulp.task('js:prod', (done) => {
    glob(config.paths.src.js + '*.js', (err, files) => {
        if (err) {
            done(err);
        }

        let tasks = files.map(file => {
            let fileName = file.split('/').pop();
            return browserify(file, {debug: true})
                .transform(babelify)
                .bundle()
                .on('error', util.log)
                .pipe(source(fileName))
                .pipe(buffer())
                .pipe(gulp.dest(config.paths.builds.tmp.js))
        });
        eventStream.merge(tasks).on('end', done);
    });
});
