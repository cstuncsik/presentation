import config from '../config.js';
import serverConfig from '../../server/config.js';
import nodemon from 'gulp-nodemon';
import browserSync from 'browser-sync';
import gulp from 'gulp';

gulp.task('serve:prod', () => {
    nodemon({
        nodeArgs: 'server',
        script: 'prod',
        watch: 'server'
    });
});
