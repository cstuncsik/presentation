import config from '../config.js';
import gulp from 'gulp';
import util from 'gulp-util';
import postcss from 'gulp-postcss';
import postcssImport from 'postcss-import';
import postcssNext from 'postcss-cssnext';
import cssnano from 'cssnano';

gulp.task('css:prod', () => {
    return gulp.src(config.paths.src.css + '+(app|controls).css')
        .pipe(postcss([
            postcssImport(),
            postcssNext(),
            cssnano()
        ]))
        .pipe(gulp.dest(config.paths.builds.tmp.css))
        .on('error', util.log);
});
