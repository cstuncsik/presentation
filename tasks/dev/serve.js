import config from '../config.js';
import serverConfig from '../../server/config.js';
import nodemon from 'gulp-nodemon';
import browserSync from 'browser-sync';
import gulp from 'gulp';

gulp.task('serve:dev', () => {

    nodemon({
        script: 'server',
        watch: 'server'
    });

    let bs =  browserSync({
        injectChanges: true,
        https: config.https,
        proxy: (config.https ? 'https' : 'http') + '://localhost:' + (config.https ? serverConfig.ports.https : serverConfig.ports.http)
    });

    gulp.watch(config.paths.src.css + '**/*.css', ['css:dev']);
    gulp.watch(config.paths.src.js + '**/*.js', ['js:dev']);
    gulp.watch(config.paths.src.root + '**/*.pug', ['pug:dev']);
    gulp.watch(config.paths.builds.root + '**/*.html').on('change', bs.reload);
});
