import config from '../config.js';
import gulp from 'gulp';
import util from 'gulp-util';
import plumber from 'gulp-plumber';

gulp.task('copy:dev', () => {
    return gulp.src(config.paths.src.fonts)
        .pipe(plumber())
        .pipe(gulp.dest(config.paths.builds.dev.fonts))
        .on('error', util.log);
});
