import config from '../config.js';
import gulp from 'gulp';
import util from 'gulp-util';
import plumber from 'gulp-plumber';

gulp.task('img:dev', () => {
    return gulp.src(config.paths.src.img)
        .pipe(plumber())
        .pipe(gulp.dest(config.paths.builds.dev.img))
        .on('error', util.log);
});
