import config from '../config.js';
import gulp from 'gulp';
import util from 'gulp-util';
import sourcemaps from 'gulp-sourcemaps';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import browserify from 'browserify';
import babelify from 'babelify';
import glob from 'glob';
import eventStream from 'event-stream';

gulp.task('js:dev', done => {
    glob(config.paths.src.js + '*.js', (err, files) => {
        if (err) {
            done(err);
        }

        let tasks = files.map(file => {
            let fileName = file.split('/').pop();
            return browserify(file, {debug: true})
                .transform(babelify)
                .bundle()
                .on('error', function (error) {
                    util.log(util.colors.red(error));
                    this.emit('end');
                })
                .pipe(source(fileName))
                .pipe(buffer())
                .pipe(sourcemaps.init({loadMaps: true}))
                .pipe(sourcemaps.write())
                .pipe(gulp.dest(config.paths.builds.dev.js))
        });
        eventStream.merge(tasks).on('end', done);
    });
});
