import path from 'path';
import _ from 'lodash';

export default (function () {

    let cfg = {};
    let args;
    let npm_config_argv = process.env.npm_config_argv;

    if (npm_config_argv) {

        npm_config_argv = JSON.parse(npm_config_argv);

        if (npm_config_argv.remain.length) {

            args = npm_config_argv.remain.map(function (item) {
                item = item.replace('--', '').split('=');
                if (!item[1]) {
                    item[1] = true;
                }
                return _.zipObject([item]);
            });

            cfg.test = _.some(args, 'test');
            cfg.https = _.some(args, 'https');

        }
    }

    cfg.paths = {};

    _.extend(cfg.paths, {
        src: {
            root: path.join('client', path.sep)
        },
        test: {
            root: path.join('test', path.sep)
        },
        builds: {
            root: path.join('builds', path.sep)
        }
    });

    cfg.dirNames = {
        js: 'js',
        css: 'css',
        img: 'img',
        fonts: 'fonts',
        html: 'html'
    };

    _.extend(cfg.paths.src, {
        js: path.join(cfg.paths.src.root, cfg.dirNames.js, path.sep),
        css: path.join(cfg.paths.src.root, cfg.dirNames.css, path.sep),
        fonts: path.join(cfg.paths.src.root, cfg.dirNames.fonts, '*'),
        img: path.join(cfg.paths.src.root, cfg.dirNames.img, '**', '*')
    });

    _.extend(cfg.paths.builds, {
        dev: {
            root: path.join(cfg.paths.builds.root, 'dev', path.sep)
        },
        prod: {
            root: path.join(cfg.paths.builds.root, 'prod', path.sep)
        },
        tmp: {
            root: path.join(cfg.paths.builds.root, 'tmp', path.sep)
        }
    });

    _.forIn(cfg.paths.builds, function (pv, pk) {
        let dirObj = cfg.paths.builds[pk];
        if(_.isObject(dirObj)){
            _.forIn(cfg.dirNames, function (dv, dk) {
                dirObj[dk] = path.join(dirObj.root, dv, path.sep)
            });
        }
    });

    return cfg;

})();
