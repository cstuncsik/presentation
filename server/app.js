import http from 'http';
import express from 'express';
import io from 'socket.io';
import config from './config.js';

const app = express();
const server = http.createServer(app);

app.use(express.static(config.clientRoot));
app.use('/bower_components', express.static(config.bowerComponents));

io(server).on('connection', socket => {

    socket.on('disconnect', () => {
        console.log("Connection " + socket.id + " terminated.");
    });

    socket.on('uievent', data => {
        socket.broadcast.emit('uievent', data);
    });

});

server.listen(config.ports.http);
