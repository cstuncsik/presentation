import path from 'path';

export default (() => {

    const args = process.argv;
    let prod = args.indexOf('prod') > -1;

    return {
        root: path.join(__dirname, '../'),
        clientRoot: path.join('builds', prod ? 'prod' : 'dev'),
        bowerComponents: path.join('bower_components'),
        serverRoot: 'server',
        ports: {
            'http': 8080,
            'https': 3443
        }
    };

})();
