# Functional Javascript Basics

HTML5 presentation (with impress.js)

##Usage

```
npm i
```

Installs dependencies.

```
npm run dev
```

It automatically opens the presentation in your default browser.
If you go to **http://localhost:8080/controls.html** with your smart phone 
you can change your slides by swiping left or right. 
It uses websocket for communication.

```
npm run build
```

Presentation starts at **http://localhost:8080** but you have to open it manually in a browser.
The rest is the same.

## License

Copyright © 2016 Csaba Tuncsik <csaba.tuncsik@gmail.com>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See [WTFPL](http://www.wtfpl.net) ![WTFPL icon](http://i.imgur.com/AsWaQQl.png) for more details.
