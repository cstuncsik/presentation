import gulp from 'gulp';
import sequence from 'run-sequence';
import requireDir from 'require-dir';

// Require production tasks
requireDir('./tasks/prod');

// Run production tasks
gulp.task('build', () => {
    sequence('clean:prod',
        [
            'copy:prod',
            'css:prod',
            'js:prod',
            'img:prod',
            'pug:prod'
        ],
        'html:prod',
        'rev:prod'
    );
});

// Require development tasks
requireDir('./tasks/dev');

// Run development tasks
gulp.task('default', () => {
    sequence('clean:dev',
        [
            'copy:dev',
            'css:dev',
            'js:dev',
            'img:dev',
            'pug:dev'
        ],
        'serve:dev'
    );
});
